## Overview

This repository contains code and data associated with the study, The Parasite Intraerythrocytic Cycle and Human Circadian Cycle are Coupled During Malaria Infection [1]. The code can be used to process the raw RNASeq data and reproduce the figures and tables reported in [1].

†Raw and processed RNASeq data from this study is available through GEO.

## Contents

This repository includes the TPM normalized time series gene expression data produced by alignment and quantification of RNASeq raw data, which is available through GEO at (TBD). See Methods in [1] for details on gene expression quantificaiton. Also included are experimental and participant metadata files for this study. The data produced in this study is compared in various ways to expression data from Bozdech _et al_, 2008 [2], and Arnardottir _et al_, 2014 [3]. The original data from these studies is provided in this repository for ease of access and to ensure the provided Python files and Jupyter notebooks reproduce the reported results.

* data/original/metadata/ - participant and experimental metadata files
* data/original/read_counts/ - counts of reads mapping to human/vivax transcriptomes
* data/original/tpm_[type]/ - original time series gene expression data for human/vivax samples in TPM units produced by RNASeq quantificaiton
* data/cleaned/tpm_qn_[type]/ - cleaned and quantile normalized time series gene expression data for human/vivax samples
* data/per/jtk_tpm_qn_[type]/ - periodicity detection results for human/vivax gene expression profiles.
* data/orthology_data/ - _P. vivax_ gene expression data reported in [2]
* data/sleepdep_data/ - Human gene expression data reported in [3]
* src/ - Python and shell scripts and Jupyter notebooks for performing data processing, periodicity detection, analyses and generating tables and figures

[type] is either 'human' or 'vivax'

## Installation

Installation assumes conda is available to be used for environment creation and Python package management. Ensure a working copy of Anaconda (https://www.anaconda.com/) is installed. This repo also depends on the JTK algorithm (git@gitlab.com:biochron/pyjtk.git) for detecting rhythmicity in gene expression. From the root directory of this repository type the following commands:

```
conda env create -f human_vivax_coupling_environment.yml
conda activate human_vivax_coupling
python -m ipykernel install --user --name=human_vivax_coupling
git clone git@gitlab.com:biochron/pyjtk.git src/pyjtk
jupyter notebook
```

The first command creates a new conda environment and installs into it `Python 3.8.5` and the exact Python packages and versions needed to run the code included in this repository. The second command activates the newly created environment. The third command makes the newly installed environment available for use within Juypyter notebooks. The fourth command clones the pyJTK code package into the `src/` folder which is needed to quantify periodicity of gene expression profiles. The final command opens a Jupyter notebook page in your web browser, which can be used to open and run the included notebooks. Ensure the kernel `human_vivax_coupling` is selected before running the notebooks.

## RNASeq Alignment and Quantification

The raw RNASeq data produced in this study, expected read count time series dataframes produced by RSEM and the final cleaned TPM and quantile normalized time series dataframes are available through GEO at (TBD). The scripts contained in `src/rnaseq_quantification_scripts/` can be used to perform read alignment, organismal disambiguation, and quantification to reproduce the TPM normalized time series expression files provided in `data/original/tpm_human/` and `data/original/tpm_vivax/` and the mapped read counts provided in `data/original/read_counts/.` Run the scripts in the order specified in `src/rnaseq_quantification_scripts/order_of_scripts.txt` and by their filename prefixes. Note: These scripts execute SLURM jobs and hardcoded directories will require updating to reflect the machine's directory structures. 

Additional cleaning and processing of time series dataframes is done by `src/data_processing.ipynb`, which outputs the final time series datafiles (contained in `data/cleaned/tpm_qn_human/` and `data/cleaned/tpm_qn_vivax/`) after performing quantile normalization, removing unexpressed genes, interpolating a missing time point, and merging PAR_Y genes from pseudoautosomal regions into single profiles.

## Data Processing and Analysis

The cells in the following notebooks can be run to reproduce the results reported in [1]:

1. `src/sleepdep/GSE56931_vivax_data_processing.ipynb` to process and analyze the data from [3] (GSE56931).
2. `src/orthology/bozdech2008_data_processing.ipynb` to process data from [2].
3. `src/data_processing.ipynb` to process metadata, generate cleaned time series files, and perform periodicity detection before the analyses reported in [1].
4. `src/figures_and_tables.ipynb` to analyze the data and generate figures and tables reported in [1].

## References

[1] Motta, F.C. _et al_ (2023). The Parasite Intraerythrocytic Cycle and Human Circadian Cycle are Coupled During Malaria Infection. Proceedings of the National Academy of Sciences (accepted).
[2] Bozdech, Z., Mok, S., Hu, G., Imwong, M., Jaidee, A., Russell, B., ..., Preiser, P. R. (2008). The transcriptome of Plasmodium vivax reveals divergence and diversity of transcriptional regulation in malaria parasites. Proceedings of the National Academy of Sciences, 105(42), 16290–16295. doi:10.1073/pnas.0807404105
[3] Arnardottir, E. S., Nikonova, E. V., Shockley, K. R., Podtelezhnikov, A. A., Anafi, R. C., Tanis, K. Q., ..., Pack, A. I. (2014). Blood-Gene Expression Reveals Reduced Circadian Rhythmicity in Individuals Resistant to Sleep Deprivation. Sleep, 37(10), 1589–1600. doi:10.5665/sleep.4064
