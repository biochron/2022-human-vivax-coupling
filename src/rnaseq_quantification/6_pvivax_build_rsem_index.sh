#!/bin/bash
#SBATCH -e /work/rcm49/AFRIMS_seqdata_processing/data/genomes/rsem_genomes/Plasmodium_vivax_RSEM/pvivax_rsem_index_build.err
#SBATCH -o /work/rcm49/AFRIMS_seqdata_processing/data/genomes/rsem_genomes/Plasmodium_vivax_RSEM/pvivax_rsem_index_build.out
#SBATCH -J pvivax_RI
#SBATCH --mem=60G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=robert.moseley@duke.edu
#SBATCH -c 16

module load STAR/2.7.5c
module load RSEM/1.3.3

# This makes STAR and RSEM indices
#rsem-prepare-reference --num-threads $SLURM_CPUS_PER_TASK --star --star-path /opt/apps/rhel7/STAR-2.7.5c/bin --gtf /work/rcm49/AFRIMS_seqdata_processing/data/genomes/Plasmodium_vivax/PlasmoDB-50_PvivaxP01.gtf /work/rcm49/AFRIMS_seqdata_processing/data/genomes/Plasmodium_vivax/PlasmoDB-50_PvivaxP01_Genome_filtered.fasta /work/rcm49/AFRIMS_seqdata_processing/data/genomes/rsem_genomes/Plasmodium_vivax_RSEM/pvivax_rsem_index/pvivax_rsem_index

# This makes just the RSEM index
rsem-prepare-reference --num-threads $SLURM_CPUS_PER_TASK --gtf /work/rcm49/AFRIMS_seqdata_processing/data/genomes/Plasmodium_vivax/PlasmoDB-50_PvivaxP01.gtf /work/rcm49/AFRIMS_seqdata_processing/data/genomes/Plasmodium_vivax/PlasmoDB-50_PvivaxP01_Genome_synced.fasta /work/rcm49/AFRIMS_seqdata_processing/data/genomes/rsem_genomes/Plasmodium_vivax_RSEM/pvivax_rsem_index/pvivax_rsem_index
