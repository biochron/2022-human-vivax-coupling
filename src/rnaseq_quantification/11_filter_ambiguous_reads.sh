#!/bin/bash
#SBATCH -e /work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/ambiguous_reads/slurm_output/filter_ambig_%j_%a.err
#SBATCH -o /work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/ambiguous_reads/slurm_output/filter_ambig_%j_%a.out
#SBATCH -J AMBIG
#SBATCH --mem=65G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=robert.moseley@duke.edu
#SBATCH -c 16
#SBATCH -a 1-16

module load samtools/1.10
module load Picard/2.18.2

ALIGNHUMANDIR=/work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/hsapiens_alignment_newstar
ALIGNVIVAXDIR=/work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/pvivax_alignment_newstar

PATIENTNUMB=$1
echo $PATIENTNUMB

# make sample directory name based on index of job array
if [ ${#SLURM_ARRAY_TASK_ID} -lt 2  ]
  then
    SAMPLEDIR="${PATIENTNUMB:(-2):8}-0${SLURM_ARRAY_TASK_ID}"
  else
    SAMPLEDIR="${PATIENTNUMB:(-2):8}-${SLURM_ARRAY_TASK_ID}"
fi
echo $SAMPLEDIR

#ALIGNFILE="${PATIENTNUMB}/${SAMPLEDIR}/Aligned.out.bam"
ALIGNFILE="${PATIENTNUMB}/${SAMPLEDIR}/Aligned.toTranscriptome.out.bam"

#UNIQALIGNFILE="${PATIENTNUMB}/${SAMPLEDIR}/Aligned.out.unique_reads.bam"
UNIQALIGNFILE="${PATIENTNUMB}/${SAMPLEDIR}/Aligned.toTranscriptome.out.unique_read.bam"

OUTPUTDIR="/work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/ambiguous_reads/${PATIENTNUMB}/${SAMPLEDIR}"

echo comm tool
LC_ALL=C comm -12 "${OUTPUTDIR}/hs_uniq_reads.txt" "${OUTPUTDIR}/pv_uniq_reads.txt" > "${OUTPUTDIR}/shared_reads.txt"
echo picard human
java -jar $PICARD FilterSamReads I="${ALIGNHUMANDIR}/${ALIGNFILE}" O="${ALIGNHUMANDIR}/${UNIQALIGNFILE}" READ_LIST_FILE="${OUTPUTDIR}/shared_reads.txt" FILTER=excludeReadList
echo picard vivax
java -jar $PICARD FilterSamReads I="${ALIGNVIVAXDIR}/${ALIGNFILE}" O="${ALIGNVIVAXDIR}/${UNIQALIGNFILE}" READ_LIST_FILE="${OUTPUTDIR}/shared_reads.txt" FILTER=excludeReadList
echo finished
