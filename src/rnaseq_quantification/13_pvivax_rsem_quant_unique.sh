#!/bin/bash
#SBATCH -e /work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/pvivax_quant_newstar_rsem_unique/slurm_output/pvivax_rsem_unique_%j_%a.err
#SBATCH -o /work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/pvivax_quant_newstar_rsem_unique/slurm_output/pvivax_rsem_unique_%j_%a.out
#SBATCH -J PV_RSEMU
#SBATCH --mem=65G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=robert.moseley@duke.edu
#SBATCH -c 16
#SBATCH -a 1-16

##################################################
# This script is run per patient.
# example command: sbatch
##################################################

module load STAR/2.7.5c
module load RSEM/1.3.3


PVRSEMGENOMEDIR=/work/rcm49/AFRIMS_seqdata_processing/data/genomes/rsem_genomes/Plasmodium_vivax_RSEM/pvivax_rsem_index/pvivax_rsem_index

FRAGMENTDATA=/work/rcm49/AFRIMS_seqdata_processing/data/2017_Smear_Analysis_Results_concat.txt

PATIENTNUMB=$1
echo $PATIENTNUMB

PATIENTDIR="/work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/pvivax_alignment_newstar/${PATIENTNUMB}"
echo $PATIENTDIR

# make sample directory name based on index of job array
if [ ${#SLURM_ARRAY_TASK_ID} -lt 2  ]
  then
    SAMPLEDIR="${PATIENTNUMB:(-2):8}-0${SLURM_ARRAY_TASK_ID}"
    FRAGLEN=$(grep -hP "${PATIENTNUMB:(-2):8}-0${SLURM_ARRAY_TASK_ID}" $FRAGMENTDATA | cut -f 7)
    FRAGSD=$(grep -hP "${PATIENTNUMB:(-2):8}-0${SLURM_ARRAY_TASK_ID}" $FRAGMENTDATA | cut -f 9)
    FRAGSD=${FRAGSD/%$'\r'/}
  else
    SAMPLEDIR="${PATIENTNUMB:(-2):8}-${SLURM_ARRAY_TASK_ID}"
    FRAGLEN=$(grep -hP "${PATIENTNUMB:(-2):8}-${SLURM_ARRAY_TASK_ID}" $FRAGMENTDATA | cut -f 7)
    FRAGSD=$(grep -hP "${PATIENTNUMB:(-2):8}-${SLURM_ARRAY_TASK_ID}" $FRAGMENTDATA | cut -f 9)
    FRAGSD=${FRAGSD/%$'\r'/}
fi
echo $SAMPLEDIR

# This is the file containing alignments to the transcriptome using star
ALIGNFILE="/work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/pvivax_alignment_newstar/${PATIENTNUMB}/${SAMPLEDIR}/Aligned.toTranscriptome.out.unique_read.bam"
echo $ALIGNFILE

# create the output directory for the RSEM output
OUTPUTDIR="/work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/pvivax_quant_newstar_rsem_unique/${PATIENTNUMB}/${SAMPLEDIR}"
echo $OUTPUTDIR
mkdir -p $OUTPUTDIR

OUTNAME="/work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/pvivax_quant_newstar_rsem_unique/${PATIENTNUMB}/${SAMPLEDIR}/${SAMPLEDIR/-}"

# with fragment stats
rsem-calculate-expression -p $SLURM_CPUS_PER_TASK --strandedness reverse --fragment-length-mean $FRAGLEN --fragment-length-sd $FRAGSD --bam --no-bam-output $ALIGNFILE $PVRSEMGENOMEDIR $OUTNAME

# use of fragment stats disabled
#rsem-calculate-expression -p $SLURM_CPUS_PER_TASK --strandedness reverse --bam --no-bam-output $ALIGNFILE $PVRSEMGENOMEDIR $OUTNAME
