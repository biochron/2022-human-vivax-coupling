#!/bin/bash
#SBATCH -e /work/rcm49/AFRIMS_seqdata_processing/output/final_output_newstar_rsem_unique/slurm_output/gather_finaldata_%j_%a.err
#SBATCH -o /work/rcm49/AFRIMS_seqdata_processing/output/final_output_newstar_rsem_unique/slurm_output/gather_finaldata_%j_%a.out
#SBATCH -J gath_un
#SBATCH --mem=30G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=robert.moseley@duke.edu
#SBATCH -a 2,8,9,10,11,13,16,17,18,19

module load RSEM/1.3.3

ALIGNHUMANDIR=/work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/hsapiens_alignment_newstar
ALIGNVIVAXDIR=/work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/pvivax_alignment_newstar

QUANTHUMANDIR=/work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/hsapiens_quant_newstar_rsem_unique
QUANTVIVAXDIR=/work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/pvivax_quant_newstar_rsem_unique

if [ ${#SLURM_ARRAY_TASK_ID} -lt 2  ]
  then
    PATIENTNUMB="0${SLURM_ARRAY_TASK_ID}"
  else
    PATIENTNUMB=$SLURM_ARRAY_TASK_ID
fi

OUTPUTDIR="/work/rcm49/AFRIMS_seqdata_processing/output/final_output_newstar_rsem_unique/Sample${PATIENTNUMB}"
mkdir -p $OUTPUTDIR/log_files
mkdir -p $OUTPUTDIR/ts_files

PATHSALIGNDIR="${ALIGNHUMANDIR}/Sample${PATIENTNUMB}"
echo $PATHSALIGNDIR
cd $PATHSALIGNDIR || exit

for SAMPLEDIR in `ls -d */`
  do
    echo $SAMPLEDIR
    cd $SAMPLEDIR || exit
    HSLOG="${SAMPLEDIR:0:5}_hs_logfinal.out"
    cp -v $PATHSALIGNDIR/${SAMPLEDIR:0:5}/Log.final.out $OUTPUTDIR/log_files/$HSLOG
    cd ..
  done


PATHSQUANTDIR="${QUANTHUMANDIR}/Sample${PATIENTNUMB}"
echo $PATHSQUANTDIR
cd $PATHSQUANTDIR || exit

for SAMPLEDIR in `ls -d */`
  do
    echo $SAMPLEDIR
    cd $SAMPLEDIR || exit
    PATSAMP1=${SAMPLEDIR/-}
    PATSAMP2=${PATSAMP1%?}
    HSGENE="$PATSAMP2.genes.results"
#    HSISO="$PATSAMP2.isoforms.results"
    NEWHSGENE="$PATSAMP2.hs.genes.results"
#    NEWHSISO="$PATSAMP2.hs.isoforms.results"
    cp -v $PATHSQUANTDIR/${SAMPLEDIR:0:5}/$HSGENE $OUTPUTDIR/ts_files/$NEWHSGENE
#    cp -v $PATHSQUANTDIR/${SAMPLEDIR:0:5}/$HSISO $OUTPUTDIR/ts_files/$NEWHSISO
    cd ..
  done

####################################################################################

PATPVALIGNDIR="${ALIGNVIVAXDIR}/Sample${PATIENTNUMB}"
echo $PATPVALIGNDIR
cd $PATPVALIGNDIR || exit

for SAMPLEDIR in `ls -d */`
  do
    echo $SAMPLEDIR
    cd $SAMPLEDIR || exit
    PVLOG="${SAMPLEDIR:0:5}_pv_logfinal.out"
    cp -v $PATPVALIGNDIR/${SAMPLEDIR:0:5}/Log.final.out $OUTPUTDIR/log_files/$PVLOG
    cd ..
  done

PATPVQUANTDIR="${QUANTVIVAXDIR}/Sample${PATIENTNUMB}"
echo $PATPVQUANTDIR
cd $PATPVQUANTDIR || exit

for SAMPLEDIR in `ls -d */`
  do
    echo $SAMPLEDIR
    cd $SAMPLEDIR || exit
    PATSAMP1=${SAMPLEDIR/-}
    PATSAMP2=${PATSAMP1%?}
    PVGENE="$PATSAMP2.genes.results"
#    PVISO="$PATSAMP2.isoforms.results"
    NEWPVGENE="$PATSAMP2.pv.genes.results"
#    NEWPVISO="$PATSAMP2.pv.isoforms.results"
    cp -v $PATPVQUANTDIR/${SAMPLEDIR:0:5}/$PVGENE $OUTPUTDIR/ts_files/$NEWPVGENE
#    cp -v $PATPVQUANTDIR/${SAMPLEDIR:0:5}/$PVISO $OUTPUTDIR/ts_files/$NEWPVISO
    cd ..
  done

# combine files to make TPM files

cd $OUTPUTDIR/ts_files
# TPM results
paste *.hs.genes.results | cut -f1,6,13,20,27,34,41,48,55,62,69,76,83,90,97,104,111 > "original_sample_${PATIENTNUMB}_tpm_human.tsv"
paste *.pv.genes.results | cut -f1,6,13,20,27,34,41,48,55,62,69,76,83,90,97,104,111 > "original_sample_${PATIENTNUMB}_tpm_vivax.tsv"


if [ "$PATIENTNUMB" = "18" ]; then
  sed -i "1s/.*/gene_id\t0\t3\t6\t9\t12\t15\t18\t21\t27\t30\t33\t36\t39\t42\t45/" "original_sample_${PATIENTNUMB}_tpm_human.tsv"
  sed -i "1s/.*/gene_id\t0\t3\t6\t9\t12\t15\t18\t21\t27\t30\t33\t36\t39\t42\t45/" "original_sample_${PATIENTNUMB}_tpm_vivax.tsv"
else
  sed -i "1s/.*/gene_id\t0\t3\t6\t9\t12\t15\t18\t21\t24\t27\t30\t33\t36\t39\t42\t45/" "original_sample_${PATIENTNUMB}_tpm_human.tsv"
  sed -i "1s/.*/gene_id\t0\t3\t6\t9\t12\t15\t18\t21\t24\t27\t30\t33\t36\t39\t42\t45/" "original_sample_${PATIENTNUMB}_tpm_vivax.tsv"
fi
