___ Concatenate files ___
0. cat_fastq_files.sh

___ Sync genome reference files with annotation files ___
1. sync_gtf_fasta_files.py

___ Convert parasite gff file to gtf file ___
2. convert_sync_files.sh

___ Build indices ___
3. hsapiens_build_star_index.sh
4. hsapiens_build_rsem_index.sh
5. pvivax_build_star_index.sh
6. pvivax_build_rsem_index.sh

___ Alignment ___
7. hsapiens_new_star_align.sh : per participant
8. pvivax_new_star_align.sh : per participant

___ Identify and Remove Ambiguous Reads then Run Cufflinks ___
9. hsapiens_get_unique_reads.sh : per participant
10. pvivax_get_unique_reads.sh : per participant
11. filter_ambiguous_reads.sh : per participant

___ Quantification - Ambiguous Reads not participant ___
12. hsapiens_rsem_quant_unique.sh : per participant
13. pvivax_rsem_quant_unique.sh : per participant

___ Gather data ___
14. gather_final_data_unique.sh : all participants : gets filtered ambiguous results

__ Combine data into 20 time series files, move to data/original/tpm_human/ and data/original/tpm_vivax/ __
15. make_norm_files.sh : per participant : uses combine_results.py : ambiguous and filtered ambiguous results

__ Count read mappings before and after removal of ambiguous reads __
read_mappings.py : all participants