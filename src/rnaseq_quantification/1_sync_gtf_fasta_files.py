#!/usr/bin/env

from Bio import SeqIO

### P. vivax PlasmoDB

# grab unique sequence names from fasta file
pv_fasta_sequences = SeqIO.parse(open('/work/rcm49/AFRIMS_seqdata_processing/data/genomes/Plasmodium_vivax/PlasmoDB-50_PvivaxP01_Genome.fasta'),'fasta')
pv_all_fasta_names = []
for fasta in pv_fasta_sequences:
    name, sequence = fasta.id, str(fasta.seq)
    pv_all_fasta_names.append(name)
pv_unique_fasta_names = list(set(pv_all_fasta_names))


# grab unique sequence names from gtf file
pv_gencode_gtf_names = []
with open('/work/rcm49/AFRIMS_seqdata_processing/data/genomes/Plasmodium_vivax/PlasmoDB-50_PvivaxP01.gtf') as f:
    for line in f:
        linesplit = line.strip().split('\t')
        if not linesplit[0].startswith('##'):
            pv_gencode_gtf_names.append(linesplit[0])
pv_gencode_gtf_unique_names = list(set(pv_gencode_gtf_names))
pv_gencode_gtf_unique_names.sort()

# Get the intersection of the two sequence name lists
pv_synced_list = list(set(pv_unique_fasta_names) & set(pv_gencode_gtf_unique_names))
pv_synced_list.sort()

### Sync Gencode fasta and gtf files on sequence names

new_pv_fasta_file = '/work/rcm49/AFRIMS_seqdata_processing/data/genomes/Plasmodium_vivax/PlasmoDB-50_PvivaxP01_Genome_synced.fasta'
pv_fasta_sequences = SeqIO.parse(open('/work/rcm49/AFRIMS_seqdata_processing/data/genomes/Plasmodium_vivax/PlasmoDB-50_PvivaxP01_Genome.fasta'),'fasta')
pv_names_kept = []
with open(new_pv_fasta_file, 'w') as out_file:
    for fasta in pv_fasta_sequences:
        name, sequence = fasta.id, str(fasta.seq)
        if name in pv_synced_list:
            pv_names_kept.append(name)
#            out_file.write(f'>{name}\n{sequence}\n')
            SeqIO.write(fasta, out_file, "fasta")
pv_unique_fasta_names_to_keep = list(set(pv_names_kept))

#pv_gtf_lines_to_keep = []
#pv_gtf_names_to_keep = []
#with open('/work/rcm49/AFRIMS_seqdata_processing/data/genomes/Plasmodium_vivax/PlasmoDB-50_PvivaxP01_synced.gtf', 'a') as outfile:
#    with open('/work/rcm49/AFRIMS_seqdata_processing/data/genomes/Plasmodium_vivax/PlasmoDB-50_PvivaxP01.gtf', 'r') as f:
#        for line in f:
#            linesplit = line.strip().split('\t')
#            name = linesplit[0]
#            if name in pv_synced_list:
#                pv_gtf_lines_to_keep.append(line.strip())
#                outfile.write(line)
#                pv_gtf_names_to_keep.append(name)
#pv_gtf_unique_names_to_keep = list(set(pv_gtf_names_to_keep))
#pv_gtf_unique_names_to_keep.sort()


### H. sapiens Gencode

# grab unique sequence names from fasta file
hs_fasta_sequences = SeqIO.parse(open('/work/rcm49/AFRIMS_seqdata_processing/data/genomes/Homo_sapiens/Gencode/GRCh38.primary_assembly.genome.fa'),'fasta')
hs_all_fasta_names = []
for fasta in hs_fasta_sequences:
    name, sequence = fasta.id, str(fasta.seq)
    hs_all_fasta_names.append(name)
hs_unique_fasta_names = list(set(hs_all_fasta_names))

# grab unique sequence names from gtf file
hs_gencode_gtf_names = []
with open('/work/rcm49/AFRIMS_seqdata_processing/data/genomes/Homo_sapiens/Gencode/gencode.v36.primary_assembly.annotation.gtf') as f:
    for line in f:
        linesplit = line.strip().split('\t')
        if not linesplit[0].startswith('##'):
            hs_gencode_gtf_names.append(linesplit[0])
hs_gencode_gtf_unique_names = list(set(hs_gencode_gtf_names))
hs_gencode_gtf_unique_names.sort()

# Get the intersection of the two sequence name lists
hs_synced_list = list(set(hs_unique_fasta_names) & set(hs_gencode_gtf_unique_names))
hs_synced_list.sort()

### Sync Gencode fasta and gtf files on sequence names

new_hs_fasta_file = '/work/rcm49/AFRIMS_seqdata_processing/data/genomes/Homo_sapiens/Gencode/GRCh38.primary_assembly.genome_synced.fa'
hs_fasta_sequences = SeqIO.parse(open('/work/rcm49/AFRIMS_seqdata_processing/data/genomes/Homo_sapiens/Gencode/GRCh38.primary_assembly.genome.fa'),'fasta')
hs_names_kept = []
with open(new_hs_fasta_file, 'w') as out_file:
    for fasta in hs_fasta_sequences:
        name, sequence = fasta.id, str(fasta.seq)
        if name in hs_synced_list:
            hs_names_kept.append(name)
#            out_file.write(f'>{name}\n{sequence}\n')
            SeqIO.write(fasta, out_file, "fasta")
hs_unique_fasta_names_to_keep = list(set(hs_names_kept))

#hs_gtf_lines_to_keep = []
#hs_gtf_names_to_keep = []
#with open('/work/rcm49/AFRIMS_seqdata_processing/data/genomes/Homo_sapiens/Gencode/gencode.v36.primary_assembly.annotation_synced.gtf', 'a') as outfile:
#    with open('/work/rcm49/AFRIMS_seqdata_processing/data/genomes/Homo_sapiens/Gencode/gencode.v36.primary_assembly.annotation.gtf', 'r') as f:
#        for line in f:
#            linesplit = line.strip().split('\t')
#            name = linesplit[0]
#            if name in hs_synced_list:
#                hs_gtf_lines_to_keep.append(line.strip())
#                outfile.write(line)
#                hs_gtf_names_to_keep.append(name)
#hs_gtf_unique_names_to_keep = list(set(hs_gtf_names_to_keep))
#hs_gtf_unique_names_to_keep.sort()

