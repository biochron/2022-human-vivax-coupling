import os
import argparse
import pandas as pd
from functools import reduce

if __name__ == '__main__':
    """
    This code combines *.genes.results or *.isofroms.results into genes.matrix.tsv and isoforms.matrix.tsv files, respectively
    """

    parser = argparse.ArgumentParser(prog='Combine RSEM Results',
                                     description='')

    # - required command-line argument -
    parser.add_argument('-d', '--top_dir',
                        action='store',
                        help='full path to directory that contains the <sample>.<species>.genes.results or <sample>.<species>.isofroms.results files.')
    parser.add_argument('-t', '--type',
                        action='store',
                        help='string specifying genes or isoforms')
    parser.add_argument('-s', '--species',
                        action='store',
                        help='string specifying pv (p. vivax) or hs (h. sapiens)')
    parser.add_argument('-o', '--output_dir',
                        action='store',
                        help='directory to save output')
    args = parser.parse_args()

    # Load and parse master config file
    top_dir = args.top_dir
    type = args.type
    species = args.species
    outdir = args.output_dir

    file_list = os.listdir(top_dir)
    type_file_list = [file for file in file_list if f'{species}.{type}.results' in file]

    counts_df_list = []
    tpm_df_list = []

    for file in type_file_list:
        timepoint = int(file.split('.')[0][2:]) * 3
        df = pd.read_csv(f'{top_dir}/{file}', sep='\t', index_col=0)
        counts_df = df[['expected_count']]
        counts_df = counts_df.rename(columns={'expected_count':timepoint})
        counts_df_list.append(counts_df)
        tpm_df = df[['TPM']]
        tpm_df = tpm_df.rename(columns={'TPM': timepoint})
        tpm_df_list.append(tpm_df)
        
    sample_id = type_file_list[0].split('.')[0][:2]

    if species == 'pv':
        species = 'vivax'
    elif species == 'hs':
        species = 'human'

    counts_df_merged = reduce(lambda left, right: pd.merge(left, right, left_index=True, right_index=True, how='outer'), counts_df_list)
    counts_df_merged_sorted = counts_df_merged.sort_index(axis=1)
    counts_df_merged_sorted.index = counts_df_merged_sorted.apply(lambda gene: gene.name.split('.')[0], axis=1) # drop ensembl ID version
    counts_df_merged_sorted = counts_df_merged_sorted[~counts_df_merged_sorted.index.duplicated(keep='first')] # drop duplicates due to 2 gene version for some genes
    counts_df_merged_sorted.to_csv(f'{outdir}/original_sample_{sample_id}_counts_{species}.tsv', sep='\t')

    tpm_df_merged = reduce(lambda left, right: pd.merge(left, right, left_index=True, right_index=True, how='outer'), tpm_df_list)
    tpm_df_merged_sorted = tpm_df_merged.sort_index(axis=1)
    tpm_df_merged_sorted.index = tpm_df_merged_sorted.apply(lambda gene: gene.name.split('.')[0], axis=1) # drop ensembl ID version
    tpm_df_merged_sorted = tpm_df_merged_sorted[~tpm_df_merged_sorted.index.duplicated(keep='first')] # drop duplicates due to 2 gene version for some genes
    tpm_df_merged_sorted.to_csv(f'{outdir}/original_sample_{sample_id}_tpm_{species}.tsv', sep='\t')

