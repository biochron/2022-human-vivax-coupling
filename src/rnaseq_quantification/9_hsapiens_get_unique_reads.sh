#!/bin/bash
#SBATCH -e /work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/ambiguous_reads/slurm_output/hs_unique_reads_%j_%a.err
#SBATCH -o /work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/ambiguous_reads/slurm_output/hs_unique_reads_%j_%a.out
#SBATCH -J HS_UR
#SBATCH --mem=60G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=robert.moseley@duke.edu
#SBATCH -c 16
#SBATCH -a 1-16

module load samtools/1.10
module load Picard/2.18.2

ALIGNHUMANDIR=/work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/hsapiens_alignment_newstar

PATIENTNUMB=$1
echo $PATIENTNUMB

# make sample directory name based on index of job array
if [ ${#SLURM_ARRAY_TASK_ID} -lt 2  ]
  then
    SAMPLEDIR="${PATIENTNUMB:(-2):8}-0${SLURM_ARRAY_TASK_ID}"
  else
    SAMPLEDIR="${PATIENTNUMB:(-2):8}-${SLURM_ARRAY_TASK_ID}"
fi
echo $SAMPLEDIR

#ALIGNFILE="${PATIENTNUMB}/${SAMPLEDIR}/Aligned.out.bam"
ALIGNFILE="${PATIENTNUMB}/${SAMPLEDIR}/Aligned.toTranscriptome.out.bam"

OUTPUTDIR="/work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/ambiguous_reads/${PATIENTNUMB}/${SAMPLEDIR}"
mkdir -p $OUTPUTDIR

# get unique reads
echo human sam
samtools view --threads $SLURM_CPUS_PER_TASK "${ALIGNHUMANDIR}/${ALIGNFILE}" | cut -f 1 | LC_ALL=C sort | uniq > "${OUTPUTDIR}/hs_uniq_reads.txt"
