#!/bin/bash
#SBATCH -e /work/rcm49/AFRIMS_seqdata_processing/output/concat_fastq_files_S2.err
#SBATCH -o /work/rcm49/AFRIMS_seqdata_processing/output/concat_fastq_files_S2.out
#SBATCH -J cat_fq
#SBATCH --mem=20G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=robert.moseley@duke.edu

PATIENTARRAY=( 02 08 09 10 11 13 16 17 18 19 )

TOPDIR=/work/rcm49/AFRIMS_seqdata_processing/data

for PATIENTNUMB in "${PATIENTARRAY[@]}"
  do
    echo $PATIENTNUMB
    PATIENTDIR="/work/rcm49/AFRIMS_seqdata_processing/data/Sample${PATIENTNUMB}"
    echo $PATIENTDIR
    cd $PATIENTDIR || exit
    for SAMPLEDIR in `ls -d */`
      do
        echo $SAMPLEDIR
        cd $SAMPLEDIR || exit
        COMBINEDFQ="${SAMPLEDIR:0:5}_combined.fastq.gz"
        echo $COMBINEDFQ
        cat *R1_001.fastq.gz > $COMBINEDFQ
        rm *R1_001.fastq
        cd ..
      done
  cd $TOPDIR || exit
  done
