#!/bin/bash
#SBATCH -e /work/rcm49/AFRIMS_seqdata_processing/data/genomes/rsem_genomes/Homo_sapiens_RSEM/hsapiens_rsem_index_build.err
#SBATCH -o /work/rcm49/AFRIMS_seqdata_processing/data/genomes/rsem_genomes/Homo_sapiens_RSEM/hsapiens_rsem_index_build.out
#SBATCH -J hsapiens_RI
#SBATCH --mem=60G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=robert.moseley@duke.edu
#SBATCH -c 16

module load STAR/2.7.5c
module load RSEM/1.3.3

# This makes STAR and RSEM indices
#rsem-prepare-reference --num-threads $SLURM_CPUS_PER_TASK --star --star-path /opt/apps/rhel7/STAR-2.7.5c/bin --gtf /work/rcm49/AFRIMS_seqdata_processing/data/genomes/Homo_sapiens/genes_filtered.gtf /work/rcm49/AFRIMS_seqdata_processing/data/genomes/Homo_sapiens/genome_filtered.fa /work/rcm49/AFRIMS_seqdata_processing/data/genomes/rsem_genomes/Homo_sapiens_RSEM/hsapiens_rsem_index/hsapiens_rsem_index

# This makes just the RSEM index
rsem-prepare-reference --num-threads $SLURM_CPUS_PER_TASK --gtf /work/rcm49/AFRIMS_seqdata_processing/data/genomes/Homo_sapiens/Gencode/gencode.v36.primary_assembly.annotation.gtf /work/rcm49/AFRIMS_seqdata_processing/data/genomes/Homo_sapiens/Gencode/GRCh38.primary_assembly.genome_synced.fa /work/rcm49/AFRIMS_seqdata_processing/data/genomes/rsem_genomes/Homo_sapiens_RSEM/hsapiens_rsem_index/hsapiens_rsem_index
