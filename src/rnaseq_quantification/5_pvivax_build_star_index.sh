#!/bin/bash
#SBATCH -e /work/rcm49/AFRIMS_seqdata_processing/data/genomes/new_star_genomes/Plasmodium_vivax_50_STAR/pvivax_index_build.err
#SBATCH -o /work/rcm49/AFRIMS_seqdata_processing/data/genomes/new_star_genomes/Plasmodium_vivax_50_STAR/pvivax_index_build.out
#SBATCH -J pvivax_SI
#SBATCH --mem=60G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=robert.moseley@duke.edu
#SBATCH -c 16

module load STAR/2.7.5c

STAR --runThreadN $SLURM_CPUS_PER_TASK --runMode genomeGenerate --genomeSAindexNbases 11 --genomeDir /work/rcm49/AFRIMS_seqdata_processing/data/genomes/new_star_genomes/Plasmodium_vivax_50_STAR/Plasmodium_vivax_index --genomeFastaFiles /work/rcm49/AFRIMS_seqdata_processing/data/genomes/Plasmodium_vivax/PlasmoDB-50_PvivaxP01_Genome_synced.fasta --sjdbGTFfile /work/rcm49/AFRIMS_seqdata_processing/data/genomes/Plasmodium_vivax/PlasmoDB-50_PvivaxP01.gtf

