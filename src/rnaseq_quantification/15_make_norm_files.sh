#!/bin/bash
#SBATCH -e /work/rcm49/AFRIMS_seqdata_processing/output/final_output_newstar_rsem/slurm_output/make_norm_files_%j_%a.err
#SBATCH -o /work/rcm49/AFRIMS_seqdata_processing/output/final_output_newstar_rsem/slurm_output/make_norm_files_%j_%a.out
#SBATCH -J norm_files
#SBATCH --mem=30G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=robert.moseley@duke.edu
#SBATCH -a 2

#,8,9,10,11,13,16,17,18,19

module load Python/3.8.1

SCRIPTSPATH=/work/rcm49/AFRIMS_seqdata_processing/scripts/new_star_scripts

if [ ${#SLURM_ARRAY_TASK_ID} -lt 2  ]
  then
    PATIENTNUMB="0${SLURM_ARRAY_TASK_ID}"
  else
    PATIENTNUMB=$SLURM_ARRAY_TASK_ID
fi

DATAPATH="/work/rcm49/AFRIMS_seqdata_processing/output/final_output_newstar_rsem_unique/Sample${PATIENTNUMB}/ts_files"

python "${SCRIPTSPATH}/combine_results.py" -d $DATAPATH -t "genes" -s "pv" -o $DATAPATH

python "${SCRIPTSPATH}/combine_results.py" -d $DATAPATH -t "genes" -s "hs" -o $DATAPATH
