#!/bin/bash
#SBATCH -e /work/rcm49/AFRIMS_seqdata_processing/data/genomes/new_star_genomes/Homo_sapiens_STAR/hsapiens_gencode_index_build.err
#SBATCH -o /work/rcm49/AFRIMS_seqdata_processing/data/genomes/new_star_genomes/Homo_sapiens_STAR/hsapiens_gencode_index_build.out
#SBATCH -J hsapiens_SI
#SBATCH --mem=60G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=robert.moseley@duke.edu
#SBATCH -c 16

module load STAR/2.7.5c

STAR --runThreadN $SLURM_CPUS_PER_TASK --runMode genomeGenerate --genomeDir /work/rcm49/AFRIMS_seqdata_processing/data/genomes/new_star_genomes/Homo_sapiens_STAR/Homo_sapiens_gencode_index --genomeFastaFiles /work/rcm49/AFRIMS_seqdata_processing/data/genomes/Homo_sapiens/Gencode/GRCh38.primary_assembly.genome_synced.fa --sjdbGTFfile /work/rcm49/AFRIMS_seqdata_processing/data/genomes/Homo_sapiens/Gencode/gencode.v36.primary_assembly.annotation.gtf

