import pandas as pd
import subprocess

TOP_DIR = '/work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/'

all_read_mappings_bam_filename = 'Aligned.toTranscriptome.out.bam'

unique_read_mappings_bam_filename = 'Aligned.toTranscriptome.out.unique_read.bam'

time_points = ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16']
participants = ['02', '08', '09', '10', '11', '13', '16', '17', '18', '19']

all_human_read_mappings_df = pd.DataFrame(index=participants, columns=time_points)
all_vivax_read_mappings_df = pd.DataFrame(index=participants, columns=time_points)

unique_in_human_read_mappings_df = pd.DataFrame(index=participants, columns=time_points)
unique_in_vivax_read_mappings_df = pd.DataFrame(index=participants, columns=time_points)

all_unique_to_human_read_mappings_df = pd.DataFrame(index=participants, columns=time_points)
all_unique_to_vivax_read_mappings_df = pd.DataFrame(index=participants, columns=time_points)

for tp in time_points:
        for sid in participants:
            path_to_all_human_reads_bam = TOP_DIR + f'hsapiens_alignment_newstar/Sample{sid}/{sid}-{tp}/{all_read_mappings_bam_filename}'
            path_to_all_vivax_reads_bam = TOP_DIR + f'pvivax_alignment_newstar/Sample{sid}/{sid}-{tp}/{all_read_mappings_bam_filename}'

            path_to_unique_human_reads_bam = TOP_DIR + f'hsapiens_alignment_newstar/Sample{sid}/{sid}-{tp}/{unique_read_mappings_bam_filename}'
            path_to_unique_vivax_reads_bam = TOP_DIR + f'pvivax_alignment_newstar/Sample{sid}/{sid}-{tp}/{unique_read_mappings_bam_filename}'


 # The dataframe containing total counts of all read mapppings loci (including multi-mappings) BEFORE any removal of ambiguous reads
            all_human_read_mappings_df.loc[sid,tp] = subprocess.check_output(f'samtools view {path_to_all_human_reads_bam} | cut -f 1 | wc -l', shell=True) 
            
            all_vivax_read_mappings_df.loc[sid,tp] = subprocess.check_output(f'samtools view {path_to_all_vivax_reads_bam} | cut -f 1 | wc -l', shell = True)
           
# The dataframe containing the total counts of the reads which map uniquely WITHIN each organisms (to a single location) before any removal of ambiguous reads
            unique_in_human_read_mappings_df.loc[sid,tp] = subprocess.check_output(f'samtools view -q 255 {path_to_all_human_reads_bam} | cut -f 1 | wc -l', shell = True) 
            unique_in_vivax_read_mappings_df.loc[sid,tp] = subprocess.check_output(f'samtools view -q 255 {path_to_all_vivax_reads_bam} | cut -f 1 | wc -l', shell = True) 
                                                         
# The dataframe containing total counts of all read mapppings loci (including multi-mappings) AFTER any removal of ambiguous reads
            all_unique_to_human_read_mappings_df.loc[sid,tp] = subprocess.check_output(f'samtools view {path_to_unique_human_reads_bam} | cut -f 1 | wc -l', shell = True) 
            all_unique_to_vivax_read_mappings_df.loc[sid,tp] = subprocess.check_output(f'samtools view {path_to_unique_vivax_reads_bam} | cut -f 1 | wc -l', shell = True) 


with open('/work/sac97/AFRIMS_ambig/all_read_mappings_human.csv', 'w') as f:
        f.write('# Number of mapping loci (including non-unique mappings) to the human transcriptome for each study participant and each time point.\n')
        all_human_read_mappings_df.to_csv(f)

with open('/work/sac97/AFRIMS_ambig/all_read_mappings_vivax.csv', 'w') as f:
         f.write('# Number of mapping loci (including non-unique mappings) to the P. vivax transcriptome for each study participant and each time point.\n')
         all_vivax_read_mappings_df.to_csv(f)


with open('/work/sac97/AFRIMS_ambig/uniquely_mapping_reads_in_human.csv', 'w') as f:
        f.write('# Number of uniquely mapping reads to the human transcriptome for each study participant and each time point.\n')
        unique_in_human_read_mappings_df.to_csv(f)
  
with open('/work/sac97/AFRIMS_ambig/uniquely_mapping_reads_in_vivax.csv', 'w') as f:
        f.write('# Number of uniquely mapping reads to the P. vivax transcriptome for each study participant and each time point.\n')
        unique_in_vivax_read_mappings_df.to_csv(f)

with open('/work/sac97/AFRIMS_ambig/all_read_mappings_unique_to_human.csv', 'w') as f:
        f.write('# Number of mapping loci (including non-unique mappings) which only mapped to the human transcriptome for each study participant and each time point.\n')
        all_unique_to_human_read_mappings_df.to_csv(f)

with open('/work/sac97/AFRIMS_ambig/all_read_mappings_unique_to_vivax.csv', 'w') as f:
        f.write('# Number of mapping loci (including non-unique mappings) which only mapped to the P. vivax transcriptome for each study participant and each time point.\n')
        all_unique_to_vivax_read_mappings_df.to_csv(f)
