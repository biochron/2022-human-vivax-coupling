#!/bin/bash
#SBATCH -e /work/rcm49/AFRIMS_seqdata_processing/data/genomes/convert_sync.err
#SBATCH -o /work/rcm49/AFRIMS_seqdata_processing/data/genomes/convert_sync.out
#SBATCH -J con_syn
#SBATCH --mem=60G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=robert.moseley@duke.edu

module load RSEM/1.3.3
module load Python/3.8.1

rsem-gff3-to-gtf /work/rcm49/AFRIMS_seqdata_processing/data/genomes/Plasmodium_vivax/PlasmoDB-50_PvivaxP01.gff /work/rcm49/AFRIMS_seqdata_processing/data/genomes/Plasmodium_vivax/PlasmoDB-50_PvivaxP01.gtf

python /work/rcm49/AFRIMS_seqdata_processing/scripts/new_star_scripts/sync_gtf_fasta_files.py


