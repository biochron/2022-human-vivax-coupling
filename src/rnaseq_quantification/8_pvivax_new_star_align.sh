#!/bin/bash
#SBATCH -e /work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/pvivax_alignment_newstar/slurm_output/pvivax_align_%j_%a.err
#SBATCH -o /work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/pvivax_alignment_newstar/slurm_output/pvivax_align_%j_%a.out
#SBATCH -J PV_NSA
#SBATCH --mem=60G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=robert.moseley@duke.edu
#SBATCH -c 16
#SBATCH -a 1-16

##################################################
# This script is run per patient.
# example command: sbatch pvivax_new_star_align.sh Sample02

# The script 'cat_fastq_files.sh' must be run before this script. 'cat_fastq_files.sh' creates the *_combined.fastq.gz file used in this script.
##################################################

module load STAR/2.7.5c

PVSTARGENOMEDIR=/work/rcm49/AFRIMS_seqdata_processing/data/genomes/new_star_genomes/Plasmodium_vivax_50_STAR/Plasmodium_vivax_index

PATIENTNUMB=$1
echo $PATIENTNUMB

PATIENTDIR="/work/rcm49/AFRIMS_seqdata_processing/data/${PATIENTNUMB}"
cd $PATIENTDIR || exit

# make sample directory name based on index of job array
if [ ${#SLURM_ARRAY_TASK_ID} -lt 2  ]
  then
    SAMPLEDIR="${PATIENTNUMB:(-2):8}-0${SLURM_ARRAY_TASK_ID}"
  else
    SAMPLEDIR="${PATIENTNUMB:(-2):8}-${SLURM_ARRAY_TASK_ID}"
fi
echo $SAMPLEDIR

cd $SAMPLEDIR || exit

# This 'combined' file is created in 'cat_fastq_files.sh'
COMBINEDFQ="${SAMPLEDIR:0:5}_combined.fastq.gz"
echo $COMBINEDFQ

# create the output directory for the STAR alignment
OUTPUTDIR="/work/rcm49/AFRIMS_seqdata_processing/output/alignment_output/pvivax_alignment_newstar/${PATIENTNUMB}/${SAMPLEDIR}/"
echo $OUTPUTDIR
mkdir -p $OUTPUTDIR

# Sorted BAM output
#STAR --runThreadN $SLURM_CPUS_PER_TASK --runMode alignReads --genomeDir $PVSTARGENOMEDIR --readFilesIn $COMBINEDFQ --quantMode TranscriptomeSAM --outSAMtype BAM SortedByCoordinate --readFilesCommand zcat --outFilterType BySJout --alignIntronMin 10 --alignIntronMax 3000 --outFileNamePrefix $OUTPUTDIR --outFilterIntronMotifs RemoveNoncanonical --outReadsUnmapped Fastx

# Unsorted BAM output for genomic cooridnates. Need to use samtools to sort. Transcriptome BAM for transcriptomic coordinates.
STAR --runThreadN $SLURM_CPUS_PER_TASK --runMode alignReads --genomeDir $PVSTARGENOMEDIR --readFilesIn $COMBINEDFQ --quantMode TranscriptomeSAM --outSAMtype BAM Unsorted --readFilesCommand zcat --outFilterType BySJout --alignIntronMin 10 --alignIntronMax 3000 --outFileNamePrefix $OUTPUTDIR --outFilterIntronMotifs RemoveNoncanonical

cd $OUTPUTDIR || exit
echo 'Files Generated:'
ls
