"""
Required utility functions and Python modules
"""
import re
import os
import csv
import json 
import pickle
import random
import cmocean
import datetime
import matplotlib
import subprocess
import numpy as np
import scipy as sp
import pandas as pd
import pingouin as pg
import seaborn as sns
import itertools as it
import scipy.stats as st
from functools import reduce
import scipy.optimize as opt
import matplotlib_venn as venn
import matplotlib.pyplot as plt
from openpyxl import load_workbook
from adjustText import adjust_text
import matplotlib.ticker as ticker
from itertools import combinations
import matplotlib.patches as patches
import matplotlib.gridspec as gridspec
from collections.abc import Iterable
from scipy.interpolate import PchipInterpolator
from scipy.stats import spearmanr
from statistics import mean, median
from collections import deque

# for saving any python object (list dictionaries)
def save_obj(obj, filepath):
    with open(filepath + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


# for loading any python object
def load_obj(filepath):
    with open(filepath + '.pkl', 'rb') as f:
        return pickle.load(f)


# for writing a list to a csv file
def save_list_to_tsv(list, filepath):
    with open(filepath, 'w') as file:
        writer = csv.writer(file, delimiter='\t')
        for item in list:
            writer.writerow([item])

            
def intersection_size(list1, list2):
    """
    returns the size of the intersection of two enumerable objects
    """
    set1 = set(list1)
    set2 = set(list2)
    
    return len(set1.intersection(set2))
   
    
def intersection_top_n(df1, df2, sid1, sid2, column='p-value', num=None, percent=None):
    """
    returns the intersections of the most periodic probes determined by either a fixed number of probes or a percentage of the \
    total numbers of probes
    :param df1: pyJTK periodicity output dataframe indexed by probes
    :param df2: pyJTK periodicity output dataframe indexed by probes
    :param column: the column used to sort probes
    :param num: the fixed number of most periodic probes to start with
    :param percent: the percentage of the total number of probes to keep
    :return: intersection dataframe
    """      
    column1 = column + '_' + sid1
    column2 = column + '_' + sid2
    
    df1_sub = df1.copy()
    df2_sub = df2.copy()

    # defaults to use percent if specified 
    if percent is not None:
        num1 = int(percent*len(df1_sub))
        num2 = int(percent*len(df2_sub))
    else:
        num1 = num
        num2 = num
        
    df1_sub.sort_values(column1, inplace=True)
    df2_sub.sort_values(column2, inplace=True)
    
    # as necessary extend num1 and num2 to capture ambiguously ranked probes
    df1_sub = df1_sub[df1_sub[column1] <= df1_sub.iloc[num1, :][column1]]
    df2_sub = df2_sub[df2_sub[column2] <= df2_sub.iloc[num2, :][column2]]
    
    intersection_set = set(df1_sub.index).intersection(set(df2_sub.index))
    return df1_sub.loc[intersection_set].join(df2_sub.loc[intersection_set], lsuffix='_1', rsuffix='_2')


def estimate_phase_shift(refer_ts, shift_ts, period):
    """
    Estimate the phase shift between two sets of N periodic time series by enumerating cyclic orderings of
    one time series to find the minimum error between the two samples of periodic orbits in R^N
    Assumes equally-spaced sampling of an integer number of periods
    :param refer_ts: Pandas DataFrame containing reference periodic time series expression profiles
    :param shift_ts: Pandas DataFrame containing periodic time series expression profiles to compare to the reference
    :param period: float specifying the assumed period of oscillation of the time series
    :returns: tuple with encoding the phase shift resulting in the minimum error between limit cycles and the minimum error value
    """
    refer_ts = refer_ts.apply(lambda x: x - x.min(), axis=1)
    refer_ts = refer_ts.apply(lambda x: x / x.max(), axis=1)
    refer_ts = refer_ts.apply(lambda x: x - x.mean(), axis=1)

    shift_ts = shift_ts.apply(lambda x: x - x.min(), axis=1)
    shift_ts = shift_ts.apply(lambda x: x / x.max(), axis=1)
    shift_ts = shift_ts.apply(lambda x: x - x.mean(), axis=1)

    time_points = [float(tp) for tp in refer_ts.columns.values]
    num_periods = int(np.ceil(max(time_points)/period))
    samp_per_period = len(time_points)//num_periods
    dt = period / samp_per_period
    
    # find time indices corresponding to the same phase under the assumed period
    phase_aligned_indices = np.array([[i + j*samp_per_period for i in range(samp_per_period)] for j in range(num_periods)])
    
    # average expression values in same phase under the assumed period to smooth the data
    refer_limit_cycle_df = pd.DataFrame()
    shift_limit_cycle_df = pd.DataFrame()
    for i in range(phase_aligned_indices.shape[1]):
        phase = time_points[phase_aligned_indices[0,i]]
        refer_limit_cycle_df[phase] = refer_ts.iloc[:, phase_aligned_indices[:,i]].mean(axis=1).values
        shift_limit_cycle_df[phase] = shift_ts.iloc[:, phase_aligned_indices[:,i]].mean(axis=1).values

    refer_limit_cycle_df.index = refer_ts.index
    shift_limit_cycle_df.index = shift_ts.index

    rmse_dict = {}
    # rotate phase of shift limit cycle against the reference and determine error as a function of the shift
    for i in range(len(shift_limit_cycle_df.columns.values)):
        reference = refer_limit_cycle_df.values
        shifted = np.roll(shift_limit_cycle_df.values, i, axis=1)
        rmse_dict[dt * i] = (((reference-shifted) ** 2).sum(axis=0).mean()) ** (0.5)

    return rmse_dict, min(rmse_dict, key=rmse_dict.get), min(rmse_dict.values())


def phase_synchronization_eigenvector(dtheta):
    """
    Estimates unknown phases theta_1,...,theta_n from noisy measurements of relative phases, theta_i-theta_j,
    using the eigenvector method of A. Singer (2009) designed to find maximizer of log likelihood in the presence of
    many outlier measurements
    :param dtheta: n-by-n skew-symmetric numpy matrix (or lower triangular numpy array) containing relative phases in [0,1)
                   with dtheta[i,j] = theta_i-theta_j + error. Expects any missing relative phases to be set to np.nan
    :return: column vector of estimated phases in theta_1,...,theta_n
    """
    # construct the Hermitian matrix H encoding phases
    H = np.exp(2 * np.pi * 1j * dtheta)
    H[np.isnan(H)] = 0
    np.fill_diagonal(H, 0)
    
    # compute the dominant eigenvector
    n = H.shape[0]
    h, v = sp.linalg.eigh(H.transpose(), eigvals=(n - 1, n - 1))

    # extract the phases of the components of the dominant eigenvectors in [0,1)
    return np.mod(np.angle(v)+np.pi, 2*np.pi).flatten() / (2*np.pi)


def circ_mean(X):
    return np.angle(np.sum(np.exp(1j*X)))


def circ_corr_perm(X, Y, num_perm=None):
    """
    computes a circular correlation coefficient and significance estimates using a permutation test
    :param X: (N,)-numpy array of circular variables assumed to be in radians
    :param Y: (N,)-numpy array of circular variables assumed to be in radians
    :param num_perm: None or integer specifying the number of permutation tests to perform. 
                     Performs exhaustive permutations if None 
    """
    X = np.asarray(X)
    Y = np.asarray(Y)
    n = len(X)
    
    # compute the resultant mean for each sample
    Xb = circ_mean(X)
    Yb = circ_mean(Y)

    Xs = np.sin(X - Xb)
    Ys = np.sin(Y - Yb)
    
    Xs2 = Xs ** 2
    Ys2 = Ys ** 2
    
    Xs2_sum = np.sum(Xs2)
    Ys2_sum = np.sum(Ys2)
    
    # compute circular correlation scaled by np.sqrt( Xs2_sum * Ys2_sum )
    r =  np.dot(Xs, Ys)
    
    count = 0
    if num_perm is None: 
        num_perm = np.math.factorial(n)
        rperms = np.zeros(num_perm)
        
        # measure correlations of all permutations
        i=0
        for perm in it.permutations(range(n)):
            perm = list(perm)
            Xs_perm = Xs[perm]

            rperm = np.dot(Xs_perm, Ys)
            rperms[i] = rperm
            i += 1
            
            if rperm >= r:
                count += 1
        
        # remove the identity permutation
        count = count - 1
        num_perm = num_perm - 1
        
    else:
        
        # measure correlations over unique random permutations
        # Warning: Does not avoid the identity permutation
        perms = []
        for i in range(num_perm):                          
            while True:                             
                perm = np.random.permutation(n)     
                key = tuple(perm)
                if key not in perms:         
                    perms.append(key)               
                    break     
                    
        rperms = np.zeros(num_perm)
        i=0
        for perm in perms:
            perm = list(perm)
            Xs_perm = Xs[perm]

            rperm = np.dot(Xs_perm, Ys)
            rperms[i] = rperm
            i += 1
            
            if rperm >= r:
                count += 1
        
    return r / np.sqrt( Xs2_sum * Ys2_sum ), count / num_perm,  rperms / np.sqrt( Xs2_sum * Ys2_sum )

    
# Define the Haase lab color scheme colormap 
norm = matplotlib.colors.Normalize(-1.5,1.5)
colors = [[norm(-1.5), "cyan"],
          [norm(0), "black"],
         [norm(1.5), "yellow"]]
haase = matplotlib.colors.LinearSegmentedColormap.from_list("", colors)


def heatmap_max(data, gene_list, first_period, yticks = False, cbar_bool=True, axis=None):
    """
    Plot time series gene expression data in a heat map, sorted using the maximum in the first period, and normalized by z-score
    
    data: data frame with gene names as the index
    gene list: list of gene names to plot
    first period = integer value of the last column of the first period
    yticks: default False doesn't plot ytick labels
    cbar_bool: default True does include color bar
    axis: default None doesn't specify an axis
    """
    data = data.loc[gene_list].copy()
    data.rename(columns={col: round(float(col)) for col in data.columns}, inplace=True)
    z_pyjtk = sp.stats.zscore(data, axis=1)
    z_pyjtk_df = pd.DataFrame(z_pyjtk, index=data.index, columns=data.columns)
    z_pyjtk_1stperiod = z_pyjtk_df.iloc[:, 0:first_period]
    max_time = z_pyjtk_1stperiod.idxmax(axis=1)
    z_pyjtk_df["max"] = max_time
    z_pyjtk_df["max"] = pd.to_numeric(z_pyjtk_df["max"])
    z_pyjtk_df = z_pyjtk_df.sort_values(by="max", axis=0)
    z_pyjtk_df = z_pyjtk_df.drop(columns=['max'])
    order = z_pyjtk_df.index
    s = sns.heatmap(z_pyjtk_df, cmap=haase, vmin=-1.5, vmax=1.5, yticklabels=yticks, cbar=cbar_bool, ax=axis)
    axis.xaxis.tick_bottom()
    axis.xaxis.set_label_position('bottom')
    return order


def heatmap_order(data, order, yticks=False, cbar_bool=True, axis=None):
    """
    Plot time series gene expression data in a heat map, sorted using the supplied order, and normalized by z-score
    
    data: data frame with gene names as the index
    order: list of genes to plot in desired order
    yticks: default False doesn't plot ytick labels
    cbar_bool: default True does include color bar
    axis: default None doesn't specify an axis
    """
    data = data.reindex(order).copy()
    data.rename(columns={col: round(float(col)) for col in data.columns}, inplace=True)
    z_pyjtk = sp.stats.zscore(data, axis=1)
    z_pyjtk_df = pd.DataFrame(z_pyjtk, index=data.index, columns=data.columns)
    order2 = z_pyjtk_df.index
    s = sns.heatmap(z_pyjtk_df, cmap=haase, vmin=-1.5, vmax=1.5, yticklabels = yticks, cbar= cbar_bool,ax = axis)
    axis.xaxis.tick_bottom()
    axis.xaxis.set_label_position('bottom')
    non_nan_df = z_pyjtk_df.dropna()
    nonnan_order = non_nan_df.index
    return order2, nonnan_order


def rose_plot(ax, angles, num_bins=16, offset=0, plot_kwargs=None):
    """
    Plot histogram of angles
    """
    # Wrap angles to [-pi, pi)
    angles = np.mod(angles + np.pi, 2*np.pi) - np.pi
    
    # Bin data and record counts
    count, bin = np.histogram(angles, bins=num_bins, range=(-np.pi, np.pi), density=True)

    # Compute width of each bin
    widths = np.diff(bin)
    
    area = count / max(count)
    # Calculate corresponding bin radius to ensure areas of slices reflect area of histogram bars
    radius = (area / np.pi)**.5
    
    # Plot data on ax
    ax.bar(bin[:-1], radius, zorder=1, align='edge', width=widths,
           **plot_kwargs)

    # Set the direction of the zero angle
    ax.set_theta_offset(offset)
    ax.set_theta_direction(-1)
    ax.set_yticks([])
    ax.set_xticks([0, np.pi/2, np.pi, 3*np.pi/2])
    ax.set_xticklabels([])
    
    
def qn_normalize(df):
    """
    Perform basic quantile normalization on a pandas dataframe
    """
    qn_df = pd.DataFrame(columns=df.columns)
    temp_df = pd.DataFrame(np.tile(df.apply(np.sort, axis=0).mean(axis=1).values, (len(df.columns),1)).transpose(), columns=df.columns)
    for col in df.columns:
        qn_df[col] = df[col].replace(to_replace=pd.DataFrame(temp_df[col].values, index=df.apply(np.sort, axis=0)[col]).groupby(col).mean().to_dict()[0])

    return qn_df


def multi_annotate(ax, s, xy_arr=[], *args, **kwargs):
    ans = []
    an = ax.annotate(s, xy_arr[0], *args, **kwargs)
    ans.append(an)
    d = {}
    try:
        d['xycoords'] = kwargs['xycoords']
    except KeyError:
        pass
    try:
        d['arrowprops'] = kwargs['arrowprops']
    except KeyError:
        pass
    for xy in xy_arr[1:]:
        an = ax.annotate(s, xy, alpha=0.0, xytext=(0,0), textcoords=an, **d)
        ans.append(an)
    return ans


def max_avg_exp_probe_drop(dataset):
    kept_probes = list()
    dataset['average'] = dataset.mean(numeric_only=True, axis=1)
    for genename, expression in dataset.groupby(dataset.index):
        if expression.shape[0] > 1:
            keep_probe_idx = expression['average'].argmax()
            kept_probes.append(expression.iloc[keep_probe_idx])
    return pd.concat(kept_probes, axis=1).T


def pairwise_gene_overlaps_jtk(data_dict, pval_range):

    # for JTK
    unique_jtk_inter_combos = [comb for comb in combinations(list(data_dict.keys()), 2)]
    jtk_inter_df_list = []

    for combo in unique_jtk_inter_combos:
        per_inter_df = pd.DataFrame(index=pval_range, columns=['df1_genes', 'df2_genes', 'Union', 'Intersection', 'Percent Overlap', 'Comparison', 'dfs'])
        df1 = data_dict[combo[0]]
        df2 = data_dict[combo[1]]

        if 'BH' not in combo[0] and 'BH' not in combo[1]:
            comparison = 'Human_vivax'
        elif 'baseline' in combo[0] and 'baseline' in combo[1]:
            comparison = 'Human_baseline'
        elif 'sleepdep' in combo[0] and 'sleepdep' in combo[1]:
            comparison = 'Human_sleepdep'
        elif ('BH' not in combo[0] and 'baseline' in combo[1]) or ('baseline' in combo[0] and 'BH' not in combo[1]):
            comparison = 'Human_vivax-Human_baseline'
        elif ('BH' not in combo[0] and 'sleepdep' in combo[1]) or ('sleepdep' in combo[0] and 'BH' not in combo[1]):
            comparison = 'Human_vivax-Human_sleepdep'
        elif ('baseline' in combo[0] and 'sleepdep' in combo[1]) or ('sleepdep' in combo[0] and 'baseline' in combo[1]):
            comparison = 'Human_baseline-Human_sleepdep'

        if 'BH' in combo[0] and 'BH' in combo[1]:
            if combo[0].split('_')[0] == combo[1].split('_')[0]:
                comparison = 'Same_Human-baseline/sleepdep'

        for pval in pval_range:
            if combo[0].startswith('BH'):
                filter_df1 = df1[df1['p-value'] <= pval]
            else:
                filter_df1 = df1[df1[f'p-value_{combo[0]}'] <= pval]
                
            if combo[1].startswith('BH'):
                filter_df2 = df2[df2['p-value'] <= pval]
            else:
                filter_df2 = df2[df2[f'p-value_{combo[1]}'] <= pval]
                
            df1_genes = filter_df1.shape[0]
            df2_genes = filter_df2.shape[0]

            union = len(set(filter_df1.index) | set(filter_df2.index))
            intersect = len(set(filter_df1.index) & set(filter_df2.index))

            if df1_genes == 0 and df2_genes == 0:
                percent_overlap = 0.0
            else:
                percent_overlap = 100*intersect/union

            per_inter_df.at[pval, 'df1_genes'] = df1_genes
            per_inter_df.at[pval, 'df2_genes'] = df2_genes
            per_inter_df.at[pval, 'Union'] = union
            per_inter_df.at[pval, 'Intersection'] = intersect
            per_inter_df.at[pval, 'Percent Overlap'] = percent_overlap
            per_inter_df.at[pval, 'Comparison'] = comparison
            per_inter_df.at[pval, 'dfs'] = combo
        jtk_inter_df_list.append(per_inter_df)

    jtk_per_inter_df = pd.concat(jtk_inter_df_list)
    jtk_per_inter_df = jtk_per_inter_df.reset_index()
    jtk_per_inter_df = jtk_per_inter_df.rename(columns={'index':'JTK p-value'})
    jtk_per_inter_df["JTK p-value"] = pd.to_numeric(jtk_per_inter_df["JTK p-value"])
    jtk_per_inter_df["df1_genes"] = pd.to_numeric(jtk_per_inter_df["df1_genes"])
    jtk_per_inter_df["df2_genes"] = pd.to_numeric(jtk_per_inter_df["df2_genes"])
    jtk_per_inter_df["Union"] = pd.to_numeric(jtk_per_inter_df["Union"])
    jtk_per_inter_df["Intersection"] = pd.to_numeric(jtk_per_inter_df["Intersection"])
    jtk_per_inter_df["Percent Overlap"] = pd.to_numeric(jtk_per_inter_df["Percent Overlap"])
    
    return jtk_per_inter_df


def section_gene_expression(data, early_mid, mid_late):

    # order genes by max expression
    data_copy = data.copy()
    max_time = data_copy.idxmax(axis=1)
    data_copy["max"] = max_time
    data_copy["max"] = pd.to_numeric(data_copy["max"])
    data_copy = data_copy.sort_values(by="max", axis=0)
    
    # slice data on max gene expression column
    early_genes = data_copy[data_copy['max'] <= early_mid]
    mid_genes = data_copy[(data_copy['max'] > early_mid) & (data_copy['max'] <= mid_late)]
    late_genes = data_copy[data_copy['max'] > mid_late]

    return early_genes.index.tolist(), mid_genes.index.tolist(), late_genes.index.tolist()

        
def load_data_dicts(DATADIR, samp_ids, samp_types, data_type, normalization, noise_thresh):
    """
    Load periodicity score dataframes and time series dataframes, filtering genes at a specified noise/amplitude threshold.
    """
    period_dict = {}
    proc_data_dict= {}
    for stype in samp_types:

        period_dict[stype] = {}
        proc_data_dict[stype] = {}
        for sid in samp_ids:

            # load the precomputed pyJTK periodicity scores
            period_dict[stype][sid] = pd.read_csv(DATADIR + '/per/jtk_%s_%s/jtk_%s_sample_%s_%s_%s.tsv' %(normalization[stype], 
                                                                                                          stype, 
                                                                                                          data_type[stype], 
                                                                                                          sid, 
                                                                                                          normalization[stype], 
                                                                                                          stype), 
                                                  delimiter='\t', 
                                                  comment='#',
                                                  index_col=0)

            # process periodicity scores to rename columns to track sample id
            column_name_mapper = {col: col + '_' + sid for col in period_dict[stype][sid].columns}
            period_dict[stype][sid].rename(columns=column_name_mapper, inplace=True)

            # load the time series data to remove genes below a speicifed threshold
            proc_data_dict[stype][sid] = pd.read_csv(DATADIR + '/%s/%s_%s/%s_sample_%s_%s_%s.tsv' %(data_type[stype],
                                                                                                    normalization[stype],
                                                                                                    stype, 
                                                                                                    data_type[stype],
                                                                                                    sid,
                                                                                                    normalization[stype],
                                                                                                    stype), 
                                                     delimiter='\t', 
                                                     comment='#',
                                                     header=0,
                                                     index_col=0)

            # remove probes whose maximum expression is less than the specified threshold
            proc_data_dict[stype][sid] = proc_data_dict[stype][sid].loc[proc_data_dict[stype][sid].max(axis=1) 
                                                                        >= noise_thresh[stype], :].copy()
            period_dict[stype][sid] = period_dict[stype][sid].loc[proc_data_dict[stype][sid].index,:].copy()
            
    return period_dict, proc_data_dict


def intersection_gene_set(lst1, lst2):
    """
    Basic intersection of two lists
    """
    lst3 = [value for value in lst1 if value in lst2]
    return lst3


def zscore_transform(data):
    z_df = sp.stats.zscore(data, axis=1)
    z_df = pd.DataFrame(z_df, index=data.index, columns=data.columns)
    return z_df


def pairwise_correlation(data_dict1, data_dict2, main_corr_dict):
    """
    Compute, as a function of the relative time shift of one against the other, the average time-point-wise Spearman correlations 
    between two time series gene expression datases. Sampling is assumed to cover an integer number of periods 
    and datasets are assumed to be sampled at the same rate. 
    If each time D1 and D2, series has N time points (columns), the average correlation is
    
         sum( corr(D1[:,i], D2[:, i+j]) )/ N, 
         
    where the sum is taken over all time points, i=1,...,N, and j indicates a fixed shift by j time points, with wrapping.
    """
    for sample1 in data_dict1.keys():
        
        s1_df = data_dict1[sample1].copy()
        
        if sample1 not in main_corr_dict.keys():
            main_corr_dict[sample1] = dict()
        for sample2 in data_dict2.keys():
            
            corr_dict = dict()
            
            s2_df = data_dict2[sample2].copy()
            
            common_gene_set = intersection_gene_set(s1_df.index, s2_df.index)
            
            s1f_df = s1_df[s1_df.index.isin(common_gene_set)]
            s2f_df = s2_df[s2_df.index.isin(common_gene_set)]
            
            s1fz_df = zscore_transform(s1f_df)
            s2fz_df = zscore_transform(s2f_df)
            
            s1fz_df = s1fz_df.sort_index()
            s2fz_df = s2fz_df.sort_index()
            
            for s1_tp in s1fz_df.columns:
                for s2_tp in s2fz_df.columns:
                    s1_tp_array = s1fz_df.loc[:, s1_tp]
                    s2_tp_array = s2fz_df.loc[:, s2_tp]
                    corr, pval = spearmanr(s2_tp_array, s1_tp_array)
                    if s2_tp in corr_dict.keys():
                        corr_dict[s2_tp].append(corr)
                    else:
                        corr_dict[s2_tp]= [corr]
            corr_df = pd.DataFrame.from_dict(corr_dict, orient='index', columns=s1fz_df.columns)

            s1_shift = deque(s1fz_df.columns.tolist())
            mean_corr_dict = dict()
            for shift in range(0, len(s1fz_df.columns.tolist())):
                s1_shift.rotate(1)
                corr_shift = list()
                for col_tp, row_tp in zip(corr_df.index, list(s1_shift)):
                    corr_shift.append(corr_df.loc[col_tp][row_tp])
                if shift == len(s1fz_df.columns.tolist()) - 1:
                    mean_corr_dict[f'{0} h'] = [list(s1_shift), mean(corr_shift)]
                else:
                    mean_corr_dict[f'{(shift+1)*3} h'] = [list(s1_shift), mean(corr_shift)]
            main_corr_dict[sample1][sample2] = mean_corr_dict
            
    return main_corr_dict